import cartReducer, { addToCart, removeFromCart, emptyCart } from "../cart.reducer";

const products = [
        {
            "id": 1,
            "name":"t-shirt",
            "price":800,
            "image":"./images/White_O_Crew_Regular_NoPocket.webp",
            "article":54768567,
            "color":"white"
        },
        {
            "id": 2,
            "name":"sweatshirt",
            "price":1500,
            "image":"./images/black-front-view-tee-sweatshirt-sweater-long-sleeve-white-background_21085-6321.avif",
            "article":7889,
            "color":"black"
        },
        {
            "id": 3,
            "name":"shirt",
            "price":1300,
            "image":"./images/shirt_noun_002_33400.jpeg",
            "article":78589,
            "color":"blue"
        },


]

describe("cartReducer", () => {
    test("should return default state", () => {

        expect(cartReducer([], { type: undefined })).toEqual([])
    });
    test("adding items to the cart", () => {
        let action = addToCart({
                "id": 4,
                "name":"jeans",
                "price":1100,
                "image":"./images/eaf54767ea56b733ba14ba6da6b7b7a3.jpeg",
                "article":65978,
                "color":"blue"
        });
        let newState = cartReducer({ cartToLocal: products }, action)
        expect(newState.cartToLocal.length).toBe(4)
    });

    test("removing from cart", () => {
        let action = removeFromCart({
            "id": 3,
            "name":"shirt",
            "price":1300,
            "image":"./images/shirt_noun_002_33400.jpeg",
            "article":78589,
            "color":"blue"
        });
        let newState = cartReducer({ cartToLocal: products }, action)
        expect(newState.cartToLocal.length).toBe(2)
    });
    test("cart clearing", () => {
        let action = emptyCart()
        let newState = cartReducer({ cartToLocal: products }, action)
        expect(newState.cartToLocal).toEqual([])
    })
});
