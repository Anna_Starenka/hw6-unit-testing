import { createContext, useState } from "react";

export const SelectContext = createContext()

const ViewProvider = ({ children }) => {
    const [ displayType, setDisplayType ] = useState("картки");

    return (
        <SelectContext.Provider value={{ displayType, setDisplayType}}>
        {children}

        </SelectContext.Provider>

    )

};

export default ViewProvider
