import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { modalClose, addToCart } from "../../reducers";
import { useContext } from "react";
import Modal from "../../Components/Modal/Modal";
import ProductCard from "../../Components/ProductCard/ProductCard";

import { SelectContext } from '../../Context/Context'

import "./Home.scss";

export default function Home() {
  const dispatch = useDispatch();

  const { displayType, setDisplayType } = useContext(SelectContext);

  const toggleDisplayType = () => {
      setDisplayType(prevDisplayType => prevDisplayType === "картки" ? "таблиця": "картки" );
  };




  const fetchData = useSelector((state) => state.data.data);
  const modal = useSelector((state) => state.modal.isModal);

  const [selectedProduct, setSelectedProduct] = useState([]);

 

  return (
    <>

        <h1 className="title-main">Clothes</h1>
        <button className="display-type-btn" onClick={toggleDisplayType}>
                {displayType === "картки" ? "Таблиця" : "Картки"}
        </button>

        {displayType === "картки" ? (
          <div className="products-wrapper">
            {!fetchData
              ? "Loading"
              : fetchData.map((product) => (
                  <ProductCard
                    key={product.id}
                    setProduct={() => setSelectedProduct(product)}
                    item={product}
                  />
                ))}
          </div>
        ):(
          <div className="products-wrapper-tabble">
            {!fetchData
              ? "Loading"
              : fetchData.map((product) => (
                  <ProductCard
                    key={product.id}
                    setProduct={() => setSelectedProduct(product)}
                    item={product}
                  />
                ))}
          </div>
        )}
            



      {modal && (
        <Modal
          text="Do you want to add this product to the cart?"
          onCancel={() => {
            dispatch(modalClose());
          }}
          onConfirm={() => {
            dispatch(addToCart(selectedProduct));
            dispatch(modalClose());
          }}
        />
      )}
    </>
  )
        }