import { render, fireEvent, screen, getByText } from "@testing-library/react";
import '@testing-library/jest-dom'

import Modal, { onCancel } from "./Modal";




describe("Modal", () => {

    let props;
    beforeEach(() => {
        props = {
            onCancel: jest.fn(),
            onConfirm: jest.fn(),
            text: "text",
        }

    })

    test("test modal fn on click", () => {
        const { getByTestId } = render(<Modal {...props} />)
        fireEvent.click(getByTestId("modal-test-id"));
        expect(props.onCancel).toHaveBeenCalledTimes(1)
    })

    test("test stopPropagation", () => {
        const mockClick = jest.fn();
        const { getByRole } = render(<Modal />);
        const elTest = getByRole("modalWrapper");
        elTest.onclick(mockClick)
        fireEvent.click(elTest)
        expect(elTest).toBeInTheDocument()
    });

    test("checking text props", () => {
        const { getByText } = render(<Modal {...props} />);
        expect(getByText("text")).toHaveTextContent("text")
    })
})

